<?php

namespace AppBundle\Entity;


use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * Usuaris
 * @ORM\Entity
 *
 * @ORM\Table(name="fos_user")
 */
class Usuaris extends BaseUser
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_usuari", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }


}

